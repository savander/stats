package pl.mediacloud.sav;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class ScoreBoardStats {
	private static DataBase conn;
	
	@SuppressWarnings("static-access")
	public static void createScoreBoard(Player p) throws SQLException{
		conn.openConnection();
		 
			p.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
			ScoreboardManager manager = Bukkit.getScoreboardManager();
			Scoreboard board = manager.getNewScoreboard();
			Objective obj = board.registerNewObjective("Statistic", "dummy");
			obj.setDisplayName("�6Statystyki");
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);
			
			PreparedStatement sql = conn.connection.prepareStatement("SELECT deads, kills FROM player_data WHERE player=?");
			sql.setString(1, p.getName());
			ResultSet rs = sql.executeQuery();
			
			while(rs.next()){
				int deadsCount = rs.getInt("deads");
				int killsCount = rs.getInt("kills");
				float kdratio = (float) killsCount/deadsCount;
				float kdr = (float) (Math.round(kdratio*100.0)/100.0);
				
				if(deadsCount == 0){
					Score kills = obj.getScore(Bukkit.getOfflinePlayer("�3Kills: �9"+ killsCount));
					Score deads = obj.getScore(Bukkit.getOfflinePlayer("�3Deaths: �9"+ deadsCount));
					Score kd = obj.getScore(Bukkit.getOfflinePlayer("�3K/D: �9"+ killsCount +".0"));
					kills.setScore(-1);
					deads.setScore(-2);
					kd.setScore(-3);
				}else{
					Score kills = obj.getScore(Bukkit.getOfflinePlayer("�3Kills: �9"+ killsCount));
					Score deads = obj.getScore(Bukkit.getOfflinePlayer("�3Deaths: �9"+ deadsCount));
					Score kd = obj.getScore(Bukkit.getOfflinePlayer("�3K/D: �9"+ kdr));
					kills.setScore(-1);
					deads.setScore(-2);
					kd.setScore(-3);
				}
				
			}
			
			p.setScoreboard(board);
			rs.close();
			sql.close();
			conn.closeConnection();

		
	}
}
