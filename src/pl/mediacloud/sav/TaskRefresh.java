package pl.mediacloud.sav;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class TaskRefresh extends BukkitRunnable {
	private static ScoreBoardStats sbs;
	private Statistic plugin; 
	public TaskRefresh(Statistic plugin) {
		this.plugin = plugin;
	}
	
	public void run(){
		Player[] list = Bukkit.getOnlinePlayers();
		for(Player p : list){
				try {
					sbs.createScoreBoard(p);
				} catch (SQLException e){
				}
		}
	}

}
