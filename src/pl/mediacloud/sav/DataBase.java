package pl.mediacloud.sav;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.configuration.file.FileConfiguration;

public class DataBase {
	public static Connection connection;
	public static FileConfiguration cfg;
	
	public synchronized static void openConnection(){
		try{
			connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/statistic", 
					"statistic", "zaq1");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public synchronized static void closeConnection(){
		try{
			connection.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public synchronized static boolean isPlayer(String player){
		try{
			PreparedStatement sql = connection.prepareStatement(
					"SELECT * FROM player_data WHERE player=?");
			sql.setString(1, player);
			ResultSet resultSet = sql.executeQuery();
			
			boolean containsPlayer = resultSet.next();
			sql.close();
			resultSet.close();
			
			return containsPlayer;
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

}
