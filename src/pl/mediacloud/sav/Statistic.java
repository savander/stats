package pl.mediacloud.sav;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Statistic extends JavaPlugin implements Listener{
	protected static final Logger Log =  Logger.getLogger("Minecraft");
	
	private static DataBase conn;
	private static ScoreBoardStats sbs;
	
	@Override
	public void onEnable() {
		Log.info(this.getName()+ " enabled");
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new TaskRefresh(this), 0, 20l*45);
		this.getCommand("stats").setExecutor(new Commands(this));
		this.getCommand("statrs").setExecutor(new Commands(this));
		this.getCommand("top10").setExecutor(new Commands(this));
		getServer().getPluginManager().registerEvents(this, this);
	}
	

	@Override
	public void onDisable() {
			try {
				if(conn.connection != null && !conn.connection.isClosed()){
					conn.connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	
	

	@SuppressWarnings("static-access")
	@EventHandler
	public void onPlayerLogin(PlayerJoinEvent evt){
		conn.openConnection();
		
		try{
			int previousLogins = 0;
			
			if(conn.isPlayer(evt.getPlayer().getName())){
				PreparedStatement sql = conn.connection.prepareStatement(
						"SELECT logins FROM player_data WHERE player=?");
				sql.setString(1, evt.getPlayer().getName());
				
				ResultSet result = sql.executeQuery();
				result.next();
				
				previousLogins = result.getInt("logins");
				
				PreparedStatement loginsUpdate = conn.connection.prepareStatement(
						"UPDATE player_data SET logins=? WHERE player=?");
				loginsUpdate.setInt(1, previousLogins + 1);
				loginsUpdate.setString(2, evt.getPlayer().getName());
				loginsUpdate.executeUpdate();
				
				loginsUpdate.close();
				sql.close();
				result.close();
				sbs.createScoreBoard(evt.getPlayer());
			}else{
				
				PreparedStatement nowyGracz = conn.connection.prepareStatement(
					"INSERT INTO player_data values(?,0,0,1);");
				nowyGracz.setString(1, evt.getPlayer().getName());
				nowyGracz.execute();
				nowyGracz.close();
				sbs.createScoreBoard(evt.getPlayer());
			
			}
		}catch(Exception e){
			e.printStackTrace();
		} finally{
			conn.closeConnection();
		}
	}
	

	@SuppressWarnings("static-access")
	@EventHandler
	public void onPlayerDead(PlayerDeathEvent evt){
		Player p = evt.getEntity().getPlayer();
		conn.openConnection();
		try{
			int previousDead = 0;
			
			if(p instanceof Player && conn.isPlayer(p.getName())){
				PreparedStatement sql = conn.connection.prepareStatement(
						"SELECT deads FROM player_data WHERE player=?");
				sql.setString(1, p.getName());
				
				ResultSet result = sql.executeQuery();
				result.next();
				
				previousDead = result.getInt("deads");
				
				PreparedStatement deadsUpdate = conn.connection.prepareStatement(
						"UPDATE player_data SET deads=? WHERE player=?");
				deadsUpdate.setInt(1, previousDead + 1);
				deadsUpdate.setString(2, p.getName());
				deadsUpdate.executeUpdate();
				
				deadsUpdate.close();
				sql.close();
				result.close();
				
			}else{
					PreparedStatement nowyGracz = conn.connection.prepareStatement(
						"INSERT INTO player_data values(?,0,0,1);");
					nowyGracz.setString(1, p.getName());
					nowyGracz.execute();
					nowyGracz.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			conn.closeConnection();
		}
		
		try{
			conn.openConnection();
			int previousKills = 0;
			if(p.getKiller() instanceof Player){
				PreparedStatement sql = conn.connection.prepareStatement(
						"SELECT kills FROM player_data WHERE player=?");
				sql.setString(1, p.getKiller().getName());
				
				ResultSet result = sql.executeQuery();
				result.next();
				
				previousKills = result.getInt("kills");
				
				PreparedStatement killsUpdate = conn.connection.prepareStatement(
						"UPDATE player_data SET kills=? WHERE player=?");
				killsUpdate.setInt(1, previousKills + 1);
				killsUpdate.setString(2,p.getKiller().getName());
				killsUpdate.executeUpdate();
				
				killsUpdate.close();
				sql.close();
				result.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			conn.closeConnection();
		}
	}

}
