package pl.mediacloud.sav;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@SuppressWarnings("static-access")
public class Commands implements CommandExecutor {
	private static DataBase conn;
	@SuppressWarnings("unused")
	private Statistic plugin; 
	public Commands(Statistic plugin) {
		this.plugin = plugin;
	}

	private static String executeQuery = "SELECT deads, kills, logins FROM player_data WHERE player=?";
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args){
		Player pl = (Player) cs;
		if(label.equalsIgnoreCase("stats")){
			if(args.length == 0){
				try{
					conn.openConnection();
				PreparedStatement sql = conn.connection.prepareStatement(executeQuery);
				sql.setString(1, pl.getPlayer().getName());
				ResultSet rs = sql.executeQuery();
				
				while(rs.next()){
					int deadsCount = rs.getInt("deads");
					int killsCount = rs.getInt("kills");
					int loginsCount = rs.getInt("logins");
					float kdratio = (float) killsCount/deadsCount;
					float kd = (float) (Math.round(kdratio*100.0)/100.0);
					if(deadsCount == 0){
						pl.sendMessage("�8Smierci: �6"+deadsCount);
						pl.sendMessage("�8Zabic: �6"+killsCount);
						pl.sendMessage("�8K/D Ratio: �6"+killsCount+".0");
						pl.sendMessage("�8Jestes u nas poraz: �6"+loginsCount);
					}else{
						pl.sendMessage("�8Smierci: �6"+deadsCount);
						pl.sendMessage("�8Zabic: �6"+killsCount);
						pl.sendMessage("�8K/D Ratio: �6"+kd);
						pl.sendMessage("�8Jestes u nas poraz: �6"+loginsCount);
					}
				}
			
				rs.close();
				sql.close();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					conn.closeConnection();
				}
				return false;
			}
			else if(args.length == 1){
				try{
					conn.openConnection();
					if(conn.isPlayer(args[0])){
						
						PreparedStatement sql = conn.connection.prepareStatement(executeQuery);
						sql.setString(1, args[0]);
						ResultSet rs = sql.executeQuery();
						
						while(rs.next()){
							int deadsCount = rs.getInt("deads");
							int killsCount = rs.getInt("kills");
							int loginsCount = rs.getInt("logins");
							float kdratio = (float) killsCount/deadsCount;
							float kd = (float) (Math.round(kdratio*100.0)/100.0);
							if(deadsCount == 0){
								String name = args[0];
								name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
								pl.sendMessage("Statystyki gracza �3"+name);
								pl.sendMessage("�8Smierci: �6"+deadsCount);
								pl.sendMessage("�8Zabic: �6"+killsCount);
								pl.sendMessage("�8K/D Ratio: �6"+killsCount+".0");
								pl.sendMessage("�8Jest u nas poraz: �6"+loginsCount);
							}else{
								String name = args[0];
								name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
								pl.sendMessage("Statystyki gracza �3"+name);
								pl.sendMessage("�8Smierci: �6"+deadsCount);
								pl.sendMessage("�8Zabic: �6"+killsCount);
								pl.sendMessage("�8K/D Ratio: �6"+kd);
								pl.sendMessage("�8Jest u nas poraz: �6"+loginsCount);
							}
						}
					}else{
						String name = args[0];
						name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
						pl.sendMessage("Gracza �3"+name+" �xnie ma w naszej bazie");
					}
				}catch(Exception e){
					e.printStackTrace();
					pl.sendMessage("Nie bylo takiego gracza na naszym serwerze!");
				}finally{
					conn.closeConnection();
				}
				return false;
			}else{
				pl.sendMessage("/stats <player_name>");
				return false;
			}
				
			
		}
		if(label.equalsIgnoreCase("statrs") && cs.isOp()){
			if(args.length == 1){
				try{
					conn.openConnection();
					if(conn.isPlayer(args[0])){
						
						PreparedStatement sql = conn.connection.prepareStatement("UPDATE player_data SET kills=0, deads=0 WHERE player=?");
						sql.setString(1, args[0]);
						sql.executeUpdate();
						
						sql.close();
						
						String name = args[0];
						name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
						pl.sendMessage("Zresetowales statystyki gracza: �3"+name);
					}else{
						String name = args[0];
						name = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
						pl.sendMessage("Gracza �3"+name+" �xnie ma w naszej bazie");
					}
				}catch(SQLException e){
					e.printStackTrace();
				}finally{
					conn.closeConnection();
				}
				
			}else{
				pl.sendMessage("Uzycie: /statrs <player_name>");
			}
			return false;
		}
		
		if(label.equalsIgnoreCase("top10")){
			pl.sendMessage("�9<--=--<-=-�9 Media-Cl PVP�9-=->--=-->");
			pl.sendMessage("Top 10 najlepszych graczy serwera");
			try{
				conn.openConnection();
				
				PreparedStatement sql = conn.connection.prepareStatement("SELECT player, kills, deads, kills/deads AS wynik FROM player_data ORDER BY kills DESC, wynik DESC, deads ASC LIMIT 15");
				ResultSet rs = sql.executeQuery();
				for(int i = 1; i<=10;i++){
					try{
						rs.next();
						String name = rs.getString("player");
						int kills = rs.getInt("kills");
						int deads = rs.getInt("deads");
						float kdratio = (float) kills/deads;
						float kd = (float) (Math.round(kdratio*100.0)/100.0);
						
						pl.sendMessage("            "+i+". �e"+name);
						pl.sendMessage("    �6Zabil: �9" + kills+" �6zginal: �9"+ deads + " �6K/D Ratio: �9" +kd);
					}catch(Exception e){}
					}
			pl.sendMessage("�9<--=--<-=-===-+_+-===-=->--=-->");
				rs.close();
				sql.close();
			}catch(SQLException e){
				e.printStackTrace();
			}finally{
				conn.closeConnection();
			}
			return false;
		}
		return false;
	}



}
